import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, Validators } from '@angular/forms';
import firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder, private afauth: AngularFireAuth) {}

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.minLength(8)]],
  });

  ngOnInit(): void {}

  onFormLogin() {
    if (this.loginForm.valid) {
      const { email, password } = this.loginForm.value;
      this.afauth
        .signInWithEmailAndPassword(email, password)
        .then((cre) => {
          if (cre.user?.emailVerified) {
            console.log('Signin complete');
          } else {
            console.log('Signin false');
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  signInWithGoogle() {
    this.afauth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }
}
