import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private fb: FormBuilder, private afauth: AngularFireAuth) {}
  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.minLength(8)]],
  });

  ngOnInit(): void {}

  onFormSubmit() {
    if (this.registerForm.valid) {
      const { email, password } = this.registerForm.value;
      this.afauth
        .createUserWithEmailAndPassword(email, password)
        .then((cre) => {
          console.log('register successfully.');
          cre.user?.sendEmailVerification();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
}
